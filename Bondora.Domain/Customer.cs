﻿using System.Collections.Generic;

namespace Bondora.Domain
{
	public class Customer: IEntity
	{
		public Customer(int id, string email, ICollection<Cart> carts)
		{
			Id = id;
			Email = email;
			Carts = carts;
		}

		public int Id { get; set; }
		public string Email { get; private set; }
		public ICollection<Cart> Carts { get; set; }

		public Cart CreateCart()
		{
			var cart = new Cart(this);
			Carts.Add(cart);
			return cart;
		}
	}
}
