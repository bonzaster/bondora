﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bondora.Domain
{
	public class Order: IEntity
	{
		public int Id { get; set; }
		public DateTime OrderDate { get; set; }
		public IEnumerable<EquipmentBase> Equipments { get; private set; }
		
		public Order()
		{
			Equipments = new List<EquipmentBase>();
		}
		public void AddItem(EquipmentBase equipment)
		{

		}
	}

	public class OrderLine:IEntity
	{
		public EquipmentBase Equipment { get; set; }
		public int Id { get; set; }
	}
}
