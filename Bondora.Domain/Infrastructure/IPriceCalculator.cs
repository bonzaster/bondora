namespace Bondora.Domain.Infrastructure
{
	public interface IPriceCalculator
	{
		int CalculatePrice(int days);
	}
}