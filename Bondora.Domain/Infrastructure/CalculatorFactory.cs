﻿using System.Collections.Generic;
using Bondora.Domain.Equipment;

namespace Bondora.Domain.Infrastructure
{
	public class CalculatorFactory
	{
		private readonly Dictionary<EquipmentType, IPriceCalculator> _priceCalculators;
		private readonly IPriceCalculator _defaultcPriceCalculator;

		// TODO find out what we should do if no calculator found for equipment
		public CalculatorFactory(Dictionary<EquipmentType, IPriceCalculator> priceCalculators, IPriceCalculator defaultcPriceCalculator = null)
		{
			_priceCalculators = priceCalculators;
			_defaultcPriceCalculator = defaultcPriceCalculator;
		}

		public IPriceCalculator GetPriceCalculator(CartItem cartItem)
		{
			IPriceCalculator priceCalculator;

			if (!_priceCalculators.TryGetValue(cartItem.Equipment.Type, out priceCalculator))
				priceCalculator = _defaultcPriceCalculator;

			return priceCalculator;
		}
	}
}