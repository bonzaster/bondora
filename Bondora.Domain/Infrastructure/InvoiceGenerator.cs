﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Bondora.Domain.Infrastructure
{
	public interface IInvoiceGenerator
	{
		Invoice GenerateInvoice(Cart cart);
	}

	public class InvoiceGenerator : IInvoiceGenerator
	{
		private readonly CalculatorFactory _calculatorFactory;

		protected InvoiceGenerator(CalculatorFactory calculatorFactory)
		{
			_calculatorFactory = calculatorFactory;
		}

		public Invoice GenerateInvoice(Cart cart)
		{
			var invoiceLines = new List<string>(cart.CartItems.Count);

			foreach (var cartItem in cart.CartItems)
			{
				var calculator = _calculatorFactory.GetPriceCalculator(cartItem);
				if (calculator == null)
					throw new NullReferenceException($"Price calculator not found for {cartItem.Equipment.Name}!");

				invoiceLines.Add($"{cartItem.Equipment.Name}\t{calculator.CalculatePrice(cartItem.Days)
				}");
			}
			//string.Join("\r\n", _invoiceLines.Select(l=> $"{l.EquipmentName}:\t{l.RentalPrice}"));
			var summary = $"Total: {invoiceLines.Sum(line => line.RentalPrice)}";

			return new Invoice($"Invoice #{cart.Id}", invoiceLines, summary);
		}
	}
}
