﻿using System.Collections.Generic;
using System.Linq;
using Bondora.Domain.Equipment;

namespace Bondora.Domain
{
	public class Cart: IEntity
	{
		public int Id { get; set; }
		public Customer Customer { get; }

		internal Cart(Customer customer)
		{
			Customer = customer;
			CartItems = new List<CartItem>();
			Id = customer.Carts.Count + 1;
		}

		public void Add(EquipmentBase equipment, int days)
		{
			// TODO what if we already got this equipment?
			// assume that in case of equipment exists we just incrementing days
			var existing = CartItems.FirstOrDefault(ci => ci.Equipment.Id == equipment.Id);

			if (existing != null)
				existing.Days += days;
			else
				CartItems.Add(new CartItem(this, equipment, days));
		}

		public ICollection<CartItem> CartItems { get; set; }
	}

	public class CartItem: IEntity
	{
		public CartItem(Cart cart, EquipmentBase equipment, int days)
		{
			Cart = cart;
			Equipment = equipment;
			Days = days;
			Id = cart.CartItems.Count + 1;
		}

		public Cart Cart { get; private set; }
		public EquipmentBase Equipment { get; private set; }
		public int Days { get;  set; }
		public int Id { get; set; }
	}
}
