﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Bondora.Domain
{
	public class Invoice
	{
		internal Invoice(int number, string title, IList<string> invoiceLines, string summary)
		{
			Lines = new ReadOnlyCollection<string>(invoiceLines);
			Number = number;
			Title = title;
			Summary = summary;
		}

		public int Number { get; private set; }
		public string Title { get; private set; }

		public IReadOnlyList<string> Lines { get; private set; } 

		public string Summary { get; private set; }
	}
}
