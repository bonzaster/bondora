﻿namespace Bondora.Domain.Equipment
{
	public enum EquipmentType
	{
		Heavy, Regular, Specialized
	}
}