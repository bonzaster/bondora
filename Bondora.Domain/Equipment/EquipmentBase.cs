﻿namespace Bondora.Domain.Equipment
{
	public abstract class EquipmentBase: IEntity
    {
	    public  EquipmentType Type { get; set; }
		public int Id { get; set; }
		public string Name { get; protected set; }
		protected EquipmentBase(int id, string name)
		{
			Id = id;
			Name = name;
		}
	}
}
