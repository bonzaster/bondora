﻿using System.Collections.Generic;

namespace Bondora.Domain.Services
{
	public enum FeeType { OneTimeFee, PremiumFee, RegularFee }

	public class FeeProvider : IFeeProvider
	{
		private readonly Dictionary<FeeType, int> _fees;
		public FeeProvider()
		{
			_fees = new Dictionary<FeeType, int>
			{
				{ FeeType.OneTimeFee, 100 },
				{ FeeType.PremiumFee, 60 },
				{ FeeType.RegularFee, 60 }
			};
		}

		public int GetFee(FeeType feeType)
		{
			if(!_fees.ContainsKey(feeType))
				throw new KeyNotFoundException($"No fee found for {feeType}");
			return _fees[feeType];
		}
	}
}
