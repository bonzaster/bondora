namespace Bondora.Domain.Services.PriceCalculators
{
	public interface IPriceCalculator
	{
		int CalculatePrice(int days);
	}
}