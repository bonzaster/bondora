namespace Bondora.Domain.Services.PriceCalculators
{
	public class SpecializedPriceCalculationVersion1 : PriceCalculatorBase
	{
		public SpecializedPriceCalculationVersion1(IFeeProvider feeProvider) : base(feeProvider){}

		public override int CalculatePrice(int days)
		{
			return FeeProvider.GetFee(FeeType.PremiumFee)
			       + (days > 3 ? (days - 3) * FeeProvider.GetFee(FeeType.RegularFee) : 0);
		}
	}
}