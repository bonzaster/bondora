namespace Bondora.Domain.Services.PriceCalculators
{
	public abstract class PriceCalculatorBase: IPriceCalculator
	{
		protected IFeeProvider FeeProvider { get; private set; }

		protected PriceCalculatorBase(IFeeProvider feeProvider)
		{
			FeeProvider = feeProvider;
		}

		public abstract int CalculatePrice(int days);
	}
}