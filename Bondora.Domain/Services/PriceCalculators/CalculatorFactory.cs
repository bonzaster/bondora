﻿using System.Collections.Generic;
using Bondora.Domain.Equipment;

namespace Bondora.Domain.Price
{
	public class CalculatorFactory
	{
		private readonly Dictionary<EquipmentType, IPriceCalculator> _strategies;
		private readonly IPriceCalculator _defaultStrategy;

		// TODO find out what we should do if no strategy found for equipment
		public CalculatorFactory(Dictionary<EquipmentType, IPriceCalculator> strategies, IPriceCalculator defaultStrategy = null)
		{
			_strategies = strategies;
			_defaultStrategy = defaultStrategy;
		}

		public IPriceCalculator GetPriceCalculator(CartItem cartItem)
		{
			IPriceCalculator strategy;

			if (!_strategies.TryGetValue(cartItem.Equipment.Type, out strategy))
				strategy = _defaultStrategy;

			return strategy;
		}
	}
}