namespace Bondora.Domain.Services.PriceCalculators
{
	public class RegularPriceCalculationVersion1 : PriceCalculatorBase
	{
		public RegularPriceCalculationVersion1(IFeeProvider feeProvider): base(feeProvider){}

		public override int CalculatePrice(int days)
		{
			return FeeProvider.GetFee(FeeType.OneTimeFee)
			       + FeeProvider.GetFee(FeeType.PremiumFee) 
			       + (days > 2 ? FeeProvider.GetFee(FeeType.RegularFee) : 0);
		}
	}
}