namespace Bondora.Domain.Services.PriceCalculators
{
	public class HeavyPriceCalculatorVersion1 : PriceCalculatorBase
	{
		public HeavyPriceCalculatorVersion1(IFeeProvider feeProvider): base(feeProvider) { }

		public override int CalculatePrice(int days)
		{
			return FeeProvider.GetFee(FeeType.OneTimeFee) 
			       + FeeProvider.GetFee(FeeType.PremiumFee)*days;
		}
	}
}