namespace Bondora.Domain.Services
{
	public interface IInvoiceGenerator
	{
		Invoice GenerateInvoice(Cart cart);
	}
}