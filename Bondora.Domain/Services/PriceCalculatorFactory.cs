﻿using System;
using System.Collections.Generic;
using Bondora.Domain.Equipment;
using Bondora.Domain.Services.PriceCalculators;

namespace Bondora.Domain.Services
{
	public class PriceCalculatorFactory
	{
		private readonly Dictionary<EquipmentType, IPriceCalculator> _priceCalculators;

		public PriceCalculatorFactory(Dictionary<EquipmentType, IPriceCalculator> priceCalculators)
		{
			if(priceCalculators == null)
				throw new ArgumentNullException(nameof(priceCalculators), "Can not be null!");

			_priceCalculators = priceCalculators;
		}

		public IPriceCalculator GetPriceCalculator(CartItem cartItem)
		{
			IPriceCalculator priceCalculator;

			if (!_priceCalculators.TryGetValue(cartItem.Equipment.Type, out priceCalculator))
				throw new KeyNotFoundException($"Price calculator not found for {cartItem.Equipment.Type}");

			return priceCalculator;
		}
	}
}