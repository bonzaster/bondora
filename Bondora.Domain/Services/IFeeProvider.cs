namespace Bondora.Domain.Services
{
	public interface IFeeProvider
	{
		int GetFee(FeeType feeType);
	}
}