﻿using System;
using System.Collections.Generic;

namespace Bondora.Domain.Services
{
	public class DefaultInvoiceGenerator : IInvoiceGenerator
	{
		private readonly PriceCalculatorFactory _priceCalculatorFactory;

		public DefaultInvoiceGenerator(PriceCalculatorFactory priceCalculatorFactory)
		{
			_priceCalculatorFactory = priceCalculatorFactory;
		}

		public Invoice GenerateInvoice(Cart cart)
		{
			var invoiceLines = new List<string>(cart.CartItems.Count);
			var total = 0;

			foreach (var cartItem in cart.CartItems)
			{
				var calculator = _priceCalculatorFactory.GetPriceCalculator(cartItem);
				if (calculator == null)
					throw new NullReferenceException($"Price calculator not found for {cartItem.Equipment.Name}!");

				var price = calculator.CalculatePrice(cartItem.Days);

				total += price;

				invoiceLines.Add($"{cartItem.Equipment.Name}\t{price}");

			}

			var summary = $"Total: {total}";

			return new Invoice(cart.Id, $"Invoice #{cart.Id}", invoiceLines, summary);
		}
	}
}
