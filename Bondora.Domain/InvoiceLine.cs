﻿namespace Bondora.Domain
{
	public class InvoiceLine
	{
		public string EquipmentName { get; set; }
		public int RentalPrice { get; set; }
	}
}