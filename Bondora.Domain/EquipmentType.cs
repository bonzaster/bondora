﻿namespace Bondora.Domain
{
	public enum EquipmentType
	{
		Heavy, Regular, Specialized
	}
}