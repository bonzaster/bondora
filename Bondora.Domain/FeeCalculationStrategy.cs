﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bondora.Domain
{
	public interface IFeeCalculationStrategy
	{
		int GetFee(int days);
	}

	public class HeavyEquipmentFeeStrategy: IFeeCalculationStrategy
	{
		public HeavyEquipmentFeeStrategy()
		{
		}

		public int GetFee(int days)
		{
			return new OneTimeFee().GetFeeValue() + days * new PremiumDailyFee().GetFeeValue();
		}
	}
}
