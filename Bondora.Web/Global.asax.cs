﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using EasyNetQ;
using LightInject;

namespace Bondora.Web
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			var container = new ServiceContainer();
			container.Register(f => RabbitHutch.CreateBus("host=localhost"), new PerRequestLifeTime());
			container.RegisterControllers();

			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			container.EnableMvc();
		}
	}
}
