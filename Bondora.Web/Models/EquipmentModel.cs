﻿using System;

namespace Bondora.Web.Models
{
	public class EquipmentModel
	{
		public int Id { get; set; }
		public String Name { get; set; }
		public int DaysRent { get; set; }
	}
}