﻿var CartViewModel = function() {
	var self = this;
	var allEquipmentsUrl = "/Equipment/GetAllEquipments";
	var cartUrl = "/Equipment/GetCartEquipments";
	var refresh = function () {
		$.ajaxSetup({ cache: false });
		$.getJSON(allEquipmentsUrl, {}, function (data) {
			self.Equipments(data);
		});
		$.getJSON(cartUrl, {}, function (data) {
			self.CartItems(data);
		});
	};

	// Public data properties
	self.Equipments = ko.observableArray([]);

	self.CartItems = ko.observableArray([]);

	// public operations
	self.addItem = function (cartItem) {
		if (cartItem.Id && cartItem.DaysRent >= 0) {
			$.ajax({
				type: 'POST',
				cache: false,
				dataType: 'json',
				url: "/Equipment/AddOrUpdateCartItem",
				data: JSON.stringify({ Id: cartItem.Id, Name: cartItem.Name, DaysRent: cartItem.DaysRent }),
				contentType: 'application/json; charset=utf-8',
				async: false,
				success: function (data) {
					window.location.href = '/Equipment';
				},
				error: function (err) {
					window.location.href = '/Equipment';
				},
				complete: function () {
				}
			});
		}
	};

	refresh();
}
ko.applyBindings(new CartViewModel());