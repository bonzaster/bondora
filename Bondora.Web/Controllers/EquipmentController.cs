﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Bondora.Shared.Dtos;
using Bondora.Shared.Dtos.Requests;
using Bondora.Shared.Dtos.Responses;
using Bondora.Web.Models;
using EasyNetQ;

namespace Bondora.Web.Controllers
{
	public class EquipmentController : Controller
	{
		private readonly IBus _bus;

		private IDictionary<int, EquipmentModel> CartItems
		{
			get
			{
				if (Session[SessionVariables.CartItems] == null)
				{
					Session[SessionVariables.CartItems] =
						new Dictionary<int, EquipmentModel>();
				}
				return Session[SessionVariables.CartItems] as IDictionary<int, EquipmentModel>;
			}
			set { Session[SessionVariables.Equipments] = value; }
		}

		private IEnumerable<EquipmentModel> Equipment
		{
			get
			{
				if (Session[SessionVariables.Equipments] == null)
				{
					Session[SessionVariables.Equipments] =
						_bus
							.Request<EquipmentListRequest, EquipmentListResponse>(new EquipmentListRequest())
							.Equipments
							.Select(e => new EquipmentModel
							{
								Id = e.Id,
								Name = e.Name,
								DaysRent = 0
							});
				}
				return Session[SessionVariables.Equipments] as IEnumerable<EquipmentModel>;
			}
		}

		public EquipmentController(IBus bus)
		{
			_bus = bus;
		}

		public JsonResult GetAllEquipments()
		{
			return Json(Equipment, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetCartEquipments()
		{
			return Json(CartItems.Values, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public HttpStatusCodeResult AddOrUpdateCartItem(EquipmentModel cartItem)
		{
			var cartItems = CartItems;
			if (cartItems.ContainsKey(cartItem.Id))
			{
				if (cartItem.DaysRent == 0)
					cartItems.Remove(cartItem.Id);
				else
					cartItems[cartItem.Id].DaysRent = cartItem.DaysRent;
			}
			else if (cartItem.DaysRent > 0) cartItems.Add(cartItem.Id, cartItem);
			return new HttpStatusCodeResult(HttpStatusCode.OK);
		}

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult GetInvoice()
		{
			if (!CartItems.Any())
				return RedirectToAction("Index");

			var cartDto = new CartDto
			{
				CustomerId = 1,
				CartItems = CartItems.Values
					.Select(c => new CartItemDto
					{
						EquipmentId = c.Id,
						DaysRent = c.DaysRent
					}).ToList()
			};

			var invoice = _bus.Request<CartDto, InvoiceDto>(cartDto);

			var sb = new StringBuilder();
			sb.AppendLine(invoice.Title);
			foreach (var line in invoice.Lines)
			{
				sb.AppendLine(line);
			}
			sb.AppendLine(invoice.Summary);

			var fileName = $"Invoice_{invoice.Number}.txt";
			MemoryStream ms = new MemoryStream();
			StreamWriter sw = new StreamWriter(ms, Encoding.Unicode);
			sw.WriteLine(sb.ToString());
			sw.Flush();
			ms.Seek(0, SeekOrigin.Begin);
			return File(ms, "application/txt", fileName);
		}
	}
}