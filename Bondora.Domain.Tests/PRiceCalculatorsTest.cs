using Bondora.Domain.Services;
using Bondora.Domain.Services.PriceCalculators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Bondora.Domain.Tests
{
	[TestClass]
	public class PriceCalculatorsTest
	{
		static int oneTimeFee = 100;
		static int premiumFee = 60;
		static int regularFee = 40;

		private static IFeeProvider feeProvider;

		[ClassInitialize]
		public static void SetUp(TestContext context)
		{
			feeProvider = Substitute.For<IFeeProvider>();
			feeProvider.GetFee(FeeType.OneTimeFee).Returns(oneTimeFee);
			feeProvider.GetFee(FeeType.PremiumFee).Returns(premiumFee);
			feeProvider.GetFee(FeeType.RegularFee).Returns(regularFee);
		}

		[TestMethod]
		public void HeavyPriceCalculatorVersion1Test()
		{
			var heavyV1 = new HeavyPriceCalculatorVersion1(feeProvider);

			var days = 1;
			var price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(days * premiumFee + oneTimeFee, price);

			days = 2;
			price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(days * premiumFee + oneTimeFee, price);

			days = 200;
			price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(days * premiumFee + oneTimeFee, price);
		}

		[TestMethod]
		public void RegularPriceCalculationVersion1Test()
		{
			var heavyV1 = new RegularPriceCalculationVersion1(feeProvider);

			var days = 1;
			var price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(premiumFee + oneTimeFee, price);

			days = 2;
			price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(premiumFee + oneTimeFee, price);

			days = 3;
			price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(regularFee + premiumFee + oneTimeFee, price);

			days = 200;
			price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(regularFee + premiumFee + oneTimeFee, price);
		}


		[TestMethod]
		public void SpecializedPriceCalculationVersion1Test()
		{
			var heavyV1 = new SpecializedPriceCalculationVersion1(feeProvider);

			var days = 1;
			var price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(premiumFee, price);

			days = 2;
			price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(premiumFee, price);

			days = 3;
			price = heavyV1.CalculatePrice(days);
			Assert.AreEqual(premiumFee, price);

			days = 200;
			price = heavyV1.CalculatePrice(days);
			Assert.AreEqual((days-3) * regularFee + premiumFee, price);
		}
	}
}