﻿using System.Collections.Generic;
using Bondora.Domain.Equipment;
using Bondora.Domain.Services;
using Bondora.Domain.Services.PriceCalculators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Bondora.Domain.Tests
{
	[TestClass]
	public class PriceCalculatorFactoryTests
	{
		[TestMethod]
		[ExpectedException(typeof(KeyNotFoundException))]
		public void PriceCalculatorFactory_NoCalculatorFound_ThrowException()
		{
			var customer = new Customer(1, "test", new List<Cart>());
			var cart = customer.CreateCart();
			var cartItem = new CartItem(cart, new HeavyEquipment(1, "Very heavy"), 1);

			var calculators = new Dictionary<EquipmentType,IPriceCalculator>();

			var pcf = new PriceCalculatorFactory(calculators);

			pcf.GetPriceCalculator(cartItem);
		}

		[TestMethod]
		public void PriceCalculatorFactory_CalculatorFound_ReturnsAppropriate()
		{
			var feeProvider = Substitute.For<IFeeProvider>();

			var customer = new Customer(1, "test", new List<Cart>());
			var cart = customer.CreateCart();
			var cartItem = new CartItem(cart, new HeavyEquipment(1, "Very heavy"), 1);

			var heavyCalculator = new HeavyPriceCalculatorVersion1(feeProvider);

			var calculators = new Dictionary<EquipmentType, IPriceCalculator>
			{
				{ EquipmentType.Heavy, heavyCalculator }
			};

			var pcf = new PriceCalculatorFactory(calculators);

			var pc = pcf.GetPriceCalculator(cartItem);

			Assert.IsInstanceOfType(pc, heavyCalculator.GetType());
		}
	}
}
