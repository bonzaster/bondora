﻿using System.Collections.Generic;
using System.Linq;
using Bondora.Domain.Equipment;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Bondora.Domain.Tests
{
	[TestClass]
	public class CartTests
	{
		[TestMethod]
		public void Cart_AddSameItem_ItemsCountNotChangedButDaysIncremented()
		{
			var c = new Customer(1, "test", new List<Cart>());
			var cart = c.CreateCart();
			var heavy = new HeavyEquipment(1, "Caterpillar");

			cart.Add(heavy, 1);
			Assert.AreEqual(1, cart.CartItems.Count);
			Assert.AreEqual(1, cart.CartItems.Single(ci => ci.Equipment.Id == heavy.Id).Days);

			cart.Add(heavy, 5);
			Assert.AreEqual(1, cart.CartItems.Count);
			Assert.AreEqual(6, cart.CartItems.Single(ci => ci.Equipment.Id == heavy.Id).Days);
		}

		[TestMethod]
		public void Cart_AddAnotherItem_DaysNotChangedButItemsCountIncremented()
		{
			var c = new Customer(1, "test", new List<Cart>());
			var cart = c.CreateCart();
			var heavy = new HeavyEquipment(1, "Caterpillar");
			var regular = new RegularEquipment(2, "KAMAZ");

			cart.Add(heavy, 1);
			Assert.AreEqual(1, cart.CartItems.Count);
			Assert.AreEqual(1, cart.CartItems.Single(ci => ci.Equipment.Id == heavy.Id).Days);

			cart.Add(regular, 5);
			Assert.AreEqual(2, cart.CartItems.Count);
			Assert.AreEqual(1, cart.CartItems.Single(ci => ci.Equipment.Id == heavy.Id).Days);
			Assert.AreEqual(5, cart.CartItems.Single(ci => ci.Equipment.Id == regular.Id).Days);
		}


	}
}
