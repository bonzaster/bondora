﻿using System;
using Bondora.Domain.Services;
using Bondora.Persistence;
using Bondora.Shared.Dtos;
using EasyNetQ;
using NLog;

namespace Bondora.Backend.Services.Responders
{
	public class InvoiceResponder : ResponderBase<CartDto, InvoiceDto>
	{
		private readonly IInvoiceGenerator _generator;

		public InvoiceResponder(IBus bus, Logger logger, IDataContext dataContext, IInvoiceGenerator generator) : 
			base(bus, logger, dataContext)
		{
			_generator = generator;
		}

		protected override Func<CartDto, InvoiceDto> Responder
		{
			get
			{
				return cartDto =>
				{
					if (cartDto.CartItems == null)
						throw new Exception("No equipments in cart!");

					var customer = DataContext.Customers.Find(cartDto.CustomerId);
					if (customer == null)
						throw new Exception($"Customer #{cartDto.CustomerId} not found!");

					var cart = customer.CreateCart();

					foreach (var cartItemDto in cartDto.CartItems)
					{
						var equipment = DataContext.Equipments.Find(cartItemDto.EquipmentId);
						cart.Add(equipment, cartItemDto.DaysRent);
					}

					DataContext.Customers.Update(customer);

					var invoice = _generator.GenerateInvoice(cart);

					// TODO use mapper
					return new InvoiceDto
					{
						Number = invoice.Number,
						Title = invoice.Title,
						Summary = invoice.Summary,
						Lines = invoice.Lines
					};
				};
			}
		}
	}
}