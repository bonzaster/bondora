﻿using System;
using System.Diagnostics;
using Bondora.Persistence;
using EasyNetQ;
using NLog;

namespace Bondora.Backend.Services.Responders
{
	public abstract class ResponderBase<TRequest, TResponse> : IResponder
		where TRequest : class where TResponse : class
	{
		protected ResponderBase(IBus bus, Logger logger, IDataContext dataContext)
		{
			_bus = bus;
			Logger = logger;
			DataContext = dataContext;
		}
		readonly IBus _bus;
		protected readonly Logger Logger;
		protected readonly IDataContext DataContext;

		protected abstract Func<TRequest, TResponse> Responder { get; }

		public void Subscribe() 
		{
			_bus.Respond<TRequest, TResponse>(r =>
			{
				var stopWatch = new Stopwatch();
				try
				{
					Logger.Trace($"processing request {r.GetType()}");
					stopWatch.Start();
					return Responder.Invoke(r);
				}
				catch (Exception e)
				{
					Logger.Error(e);
					throw;
				}
				finally
				{
					stopWatch.Stop();
					Logger.Trace($"request {r.GetType()} processed. Elapsed {stopWatch.ElapsedMilliseconds} ms");
				}
			});
		}
	}
}