﻿using System;
using System.Linq;
using Bondora.Persistence;
using Bondora.Shared.Dtos;
using Bondora.Shared.Dtos.Requests;
using Bondora.Shared.Dtos.Responses;
using EasyNetQ;
using NLog;

namespace Bondora.Backend.Services.Responders
{
	public class EquipmentListResponder : ResponderBase<EquipmentListRequest, EquipmentListResponse>
	{
		public EquipmentListResponder(IBus bus, Logger logger, IDataContext dataContext)
			:base(bus, logger, dataContext) { }

		protected override Func<EquipmentListRequest, EquipmentListResponse> Responder
		{
			get
			{
				return r =>
				{
					return new EquipmentListResponse
					{
						Equipments = DataContext.Equipments.SelectAll()
							// TODO use mapper
							.Select(e => new EquipmentDto
							{
								Id = e.Id,
								Name = e.Name
							})
							.ToList()
					};
				};
			}
		}
	}
}