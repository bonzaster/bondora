﻿namespace Bondora.Backend.Services.Responders
{
	public interface IResponder
	{
		void Subscribe();
	}
}
