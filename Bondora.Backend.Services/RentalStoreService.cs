﻿using System.Collections.Generic;
using Bondora.Backend.Services.Responders;

namespace Bondora.Backend.Services
{
	public class RentalStoreService 
	{
		private readonly IEnumerable<IResponder> _responders;

		public RentalStoreService(IEnumerable<IResponder> responders)
		{
			_responders = responders;
		}

		public void StartService()
		{
			foreach (var responder in _responders)
			{
				responder.Subscribe();
			}
		}
	}
}
