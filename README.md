# Bondora.RentalStore
Home assignment for Bondora AS

Instructions to setup the environment:

* Download and install [Erlang OTP](http://www.erlang.org/download.html)
* Download and install [RabbitMQ](http://www.rabbitmq.com/download.html)

Instructions to run the application:

1. Clone the repo
1. Restore the packages
1. Set **Bondora.Backend.Console** and **Bondora.Web** as startup projects.
1. Run the app