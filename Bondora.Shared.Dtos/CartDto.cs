﻿using System.Collections.Generic;

namespace Bondora.Shared.Dtos
{
    public class CartDto
    {
	    public int CustomerId { get; set; }
		public IEnumerable<CartItemDto> CartItems { get; set; }
    }

	public class CartItemDto
	{
		public int EquipmentId { get; set; }
		public int DaysRent { get; set; }
	}
}
