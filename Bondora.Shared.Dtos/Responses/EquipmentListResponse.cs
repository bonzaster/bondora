﻿using System.Collections.Generic;

namespace Bondora.Shared.Dtos.Responses
{
	public class EquipmentListResponse
	{
		public IList<EquipmentDto> Equipments { get; set; }
	}
}
