﻿using System.Collections;
using System.Collections.Generic;

namespace Bondora.Shared.Dtos
{
	public class InvoiceDto
	{
		public int Number { get; set; }
		public string Title { get; set; }
		public IEnumerable<string> Lines { get; set; }
		public string Summary { get; set; }
	}
}
