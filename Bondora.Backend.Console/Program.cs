﻿using System.Collections.Generic;
using Bondora.Backend.Services;
using Bondora.Backend.Services.Responders;
using Bondora.Domain;
using Bondora.Domain.Equipment;
using Bondora.Domain.Services;
using Bondora.Domain.Services.PriceCalculators;
using Bondora.Persistence;
using EasyNetQ;
using LightInject;
using NLog;

namespace Bondora.Backend.Console
{
	class Program
	{
		private static Dictionary<int, EquipmentBase> _equipments;
		private static Dictionary<int, Customer> _customers;
		private static Dictionary<EquipmentType, IPriceCalculator> _calculators;

		public static void Initialize()
		{
			_equipments = new Dictionary<int, EquipmentBase>
			{
				{1, new HeavyEquipment(1, "Caterpillar bulldozer")},
				{2, new RegularEquipment(2, "KamAZ truck")},
				{3, new HeavyEquipment(3, "Komatsu crane")},
				{4, new RegularEquipment(4, "Volvo steamroller")},
				{5, new SpecializedEquipment(5, "Bosch jackhammer")}
			};

			_customers = new Dictionary<int, Customer>
			{
				{1, new Customer(1, "customer@bondora.ee", new List<Cart>()) }
			};

			_calculators = new Dictionary<EquipmentType, IPriceCalculator>
			{
				
			};
		}

		static void Main(string[] args)
		{
			Initialize();

			using (var container = new ServiceContainer(new ContainerOptions { EnableVariance = false}))
			{
				container.RegisterAssembly("*.dll");

				using (var bus = RabbitHutch.CreateBus("host=localhost"))
				{
					container.RegisterInstance(LogManager.GetLogger("Program"));
					container.RegisterInstance(bus);
					container.RegisterInstance(
						new Dictionary<EquipmentType, IPriceCalculator>
						{
							{ EquipmentType.Heavy, new HeavyPriceCalculatorVersion1(container.GetInstance<IFeeProvider>()) }
						});

					container.Register<IRepository<Customer>>(f => new InMemoryRepository<Customer>(_customers));
					container.Register<IRepository<EquipmentBase>>(f => new InMemoryRepository<EquipmentBase>(_equipments));


					var responders = container.GetAllInstances<IResponder>();

					var service = new RentalStoreService(responders);
					service.StartService();
					System.Console.WriteLine("Service started. Press Enter to exit...");
					System.Console.ReadLine();
				}
			}
		}
	}
}
