using System.Collections.Generic;
using Bondora.Domain;

namespace Bondora.Persistence
{
	public sealed class InMemoryRepository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity
	{
		#region Private Fields

		private readonly Dictionary<int, TEntity> _storage;

		#endregion Private Fields

		public InMemoryRepository()
		{
			_storage = new Dictionary<int, TEntity>();
		}

		public InMemoryRepository(Dictionary<int, TEntity> storage)
		{
			_storage = storage;
		}

		public TEntity Find(int id)
		{
			TEntity t;
			_storage.TryGetValue(id, out t);
			return t;
		}

		public void Create(TEntity entity)
		{
			_storage.Add(entity.Id, entity);
		}

		public void Update(TEntity entity)
		{
			if (_storage.ContainsKey(entity.Id))
				_storage[entity.Id] = entity;
		}

		public void Delete(int id)
		{
			if (_storage.ContainsKey(id))
				_storage.Remove(id);
		}

		public IEnumerable<TEntity> SelectAll()
		{
			return _storage.Values;
		}
	}
}