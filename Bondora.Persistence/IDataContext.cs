﻿using Bondora.Domain;
using Bondora.Domain.Equipment;

namespace Bondora.Persistence
{
	public interface IDataContext
	{
		IRepository<Customer> Customers { get; set; }
		IRepository<EquipmentBase> Equipments { get; set; } 
	}
}