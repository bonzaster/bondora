﻿using Bondora.Domain;
using Bondora.Domain.Equipment;

namespace Bondora.Persistence
{
	public class InMemoryDataContext : IDataContext
	{
		public IRepository<Cart> Carts { get; set; }
		public IRepository<Customer> Customers { get; set; }
		public IRepository<EquipmentBase> Equipments { get; set; }
	}
}
