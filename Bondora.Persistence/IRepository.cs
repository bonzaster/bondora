﻿using System.Collections.Generic;
using Bondora.Domain;

namespace Bondora.Persistence
{
	public interface IRepository<TEntity> where TEntity : class, IEntity
	{
		TEntity Find(int id);
		void Create(TEntity entity);
		void Update(TEntity entity);
		void Delete(int id);
		IEnumerable<TEntity> SelectAll();
	}
}
